
# Shortname

> Shorten names to initials, it doesn't fit container


## Getting Started


In your web page:

```html
  <!-- Define some blocks with names in -->
  <div class="namefield">Joakim von Anka</div>
  <div class="namefield">Simon Dahlberg</div>
  <div class="namefield"> Will not be acted on</div>
  <div class="namefield"><b>Will also not be acted on</b></div>
  <div class="namefield">Nils</div>

  <!-- Load library -->
  <script src="./dist/shortname.js"></script>

  <script>
    // Use library on all elements with a `namefield` class
    // Internally a document.querySelectors is used to modify the elements
    shortname('.namefield', {
      guard: /^( |<)/, // Don't act on names matching regular expression
    })
  </script>
```

Usage: shortname(selector, config)

### Config ptions

**guard**

Can be a string, regex or a function
  
  type     | definition
  ---------|-----------------------------------------------------------------------------------------
  string   | Don't act on name, if it's prefixed with given string                                    
  regex    | Don't act on name if it matches the given regex                                          
  function | function is called with the name as argument. Don't act on name if function returns true

**useDot**

  type    | definition
  --------|------------------------------------------------------------------------
  Boolean | If true (default), end each shorted word with a dot, otherwise don't

## License

MIT (c) Simon Dahlberg
