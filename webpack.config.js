const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'shortname.js',
    library: 'shortname',
    libraryTarget: 'umd',
    libraryExport: 'default',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    contentBase: './dist',
    hot: true,
  },
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html', inject: 'head'}),
  ],
}


    //"babel-core": "^6.26.0",
    //"babel-loader": "^7.1.2",
    //"html-webpack-plugin": "^3.2.0",
    //"webpack": "^3.8.1",
    //"webpack-dev-server": "^3.1.3"
