import * as ut from './utils'

// shortenName
//     returns a name with initials and last name.
//     i.e. 'Joakim Von Anka' -> 'J. V. Anka'
function shortenName(name, punctuation) {
  if(!!!name) {
    return ""
  }

  let parts = ut.splitWords(name)

  if(parts.length < 2) {
    return name //ut.normalizeNameCase(name);
  }

  let names = parts.slice(0, parts.length-1)
  let lastname = parts[parts.length-1]
  let joiner = punctuation + ' '

  return ut.initials(names).join(joiner) + joiner + lastname //ut.normalizeNameCase(lastname);
}

// reallyShortenName
//     returns Only initials of name. Case is normalized.
//     'Joakim Von Anka' -> 'J. V. A.'
function reallyShortenName(name, punctuation) {
  if(!!!name) {
    return "";
  }

  if(ut.splitWords(name).length < 2){
    return name;
  }

  return ut.compose(
    ut.join.bind(this, punctuation + ' ', punctuation),
    //ut.map.bind(this, ut.normalizeNameCase),
    ut.initials,
    ut.splitWords)(name);
}

// evenShorterName
//     returns First and Last name intact, and shortens the middle names
function shortenMiddlenames(name, punctuation) {
  if(!!!name) {
    return ""
  }

  let parts = ut.splitWords(name)
  if(parts.length < 3) {
    return name
  }

  let firstName = parts[0]
  let lastName = parts[parts.length-1]
  let middleNames = ut.compose(
      ut.join.bind(this, punctuation + ' ', punctuation),
      ut.initials,
  )(parts.slice(1, parts.length-1))

  return [firstName, middleNames, lastName].join(' ')
}

const regexGuard = (re) => (name) => Boolean(name.match(re))
const prefixGuard = (prefix) => (name) => name.startsWith(prefix)
const fnGuard = (fn) => (name) => fn(name)

function getGuard(predicate) {
  if(typeof predicate === typeof '')
    return prefixGuard(predicate)

  else if(typeof predicate === 'function')
    return fnGuard(predicate)

  else if(predicate instanceof RegExp)
    return regexGuard(predicate)

  else
    return (name) => true
}

const DEFAULT_CONFIG = {
  guard: (name) => true, // Never disables the functionality
  useDot: true, // End shortings with punctuation
}

class Shortname {
  constructor(config) {
    this.config = Object.assign({}, DEFAULT_CONFIG, config)
    this.guard = getGuard(config.guard)
    this.punctuation = (!!config.useDot ? '.' : '')

    this.elements = []
    // Bind events
    window.addEventListener('resize', () => { this.fixNames() })
  }


  fixNames() {
    this.elements.forEach(({node, spanNode, textWidth, nameVersions}) => {
      let containerWidth = node.offsetWidth

      for(let i=0; i<nameVersions.length; ++i) {
        let name = nameVersions[i]
        spanNode.innerHTML = name
        if(spanNode.offsetWidth < containerWidth) {
          break
        }
      }

    })
  }

  addNode(node) {
    let originalName = node.innerHTML
    if(this.guard(originalName)) {
      return
    }

    let spanNode = document.createElement('span')
    spanNode.innerHTML = originalName
    spanNode.style.whiteSpace = 'nowrap'

    // Delete all old children
    while(node.firstChild) {
      node.removeChild(node.firstChild)
    }
    // Append the new
    node.appendChild(spanNode)

    // Calculate the width of original text
    let textWidth = spanNode.offsetWidth

    this.elements.push({
      node,
      spanNode,
      textWidth,
      originalName,
      nameVersions: [
        originalName,
        shortenMiddlenames(originalName, this.punctuation),
        shortenName(originalName, this.punctuation),
        reallyShortenName(originalName, this.punctuation),
      ],
    })
  }
}

export default function shortname(selector, config) {
  let shortnameInstance = new Shortname(config)

  let nodes = document.querySelectorAll(selector)
  Array.prototype.forEach.call(nodes, (node)=>{
    shortnameInstance.addNode(node)
  })

  shortnameInstance.fixNames()
}
