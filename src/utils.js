
// Compose a function from smaller functions
// such as compose(a, b, c) -> a(b(c)))
export function compose() {
  let fns = arguments;

  return function(result) {
    for(let i=fns.length-1; i >= 0; --i) {
      result = fns[i].call(this, result);
    }
    return result;
  }
}

// Join array with words using str.
export function join(str, endStr, words) {
  return words.join(str) + endStr;
}

// Execute function on every item in array.
//   Shorthand for Array.map
export function map(fn, arr) {
  return arr.map(fn);
}

// firstLetter
//     returns the first letter of a word
export function firstLetter(word) {
  let parts = word.split('-').map(part => part[0])
  return parts.join('-')
}

// normalizeNameCase
//     returns text with first letter uppercase, other letters lowercase
export function normalizeNameCase(text) {
  return text[0].toUpperCase() + text.slice(1).toLowerCase();
}

// initials
//     function that takes an array with words, and returns the first
//     letter of each word. Case is uppercase
export function initials(words) {
  return words.map(compose(/*normalizeNameCase,*/ firstLetter));
}

// splitWords
//     Split sentence into words. sentence is split on one or more whitespace charachters
export function splitWords(sentence) {
  return sentence.split(/\s+/);
}
